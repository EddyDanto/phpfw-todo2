<?php

use App\Task;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TasksTableSeeder extends Seeder {

  public function run()
  {
    DB::table('tasks')->delete();

    for($i=1;$i<10;$i++) {
      Task::create([
        'name' => "Task $i",
        'slug' => "task-$i",
        'project_id' => rand(1,4),
        'completed' => false,
        'notes' => "This is task #$i!"
      ]);
    }
  }

}