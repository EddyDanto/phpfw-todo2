@extends('layouts.main')

@section('content')
    <h2>Project: {{ $project->name }}</h2>
    <a href="{{ route('projects.index') }}">Back to projects</a>
    <hr/>
    <h3>Tasks:</h3>
    @if ($project->tasks->count())
        <ul>
            @foreach ($project->tasks as $task)
                <li>
                    <a href="{{ route('projects.tasks.show', [$project->slug, $task->slug]) }}">{{ $task->name }}</a>
                </li>
            @endforeach
        </ul>
    @else
        <p>
            There were no tasks found.
        </p>
    @endif
@stop

